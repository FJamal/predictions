<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("login_view.php");
    }
    else if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $user = $_POST["user"];
        $password = $_POST["password"];

        if(empty($user) && empty($password))
        {
            render("login_view.php", ["loginError" => true]);
        }
        else
        {
            //making request to db
            try
            {
                $sql = $dbh->prepare("SELECT * FROM users WHERE username = :username");
                $sql->execute([":username" => $user ]);
                $row = $sql->fetch(PDO::FETCH_ASSOC);
                //print_r($row);

            }
            catch(PDOException $e)
            {
                print("Error in Select" . $e->getMessage());
            }
            //if user exsists and password is correct
            if($row)
            {
                //verify password
                if (password_verify($password, $row["password"]))
                {
                    $_SESSION["id"] = $row["user_id"];

                    redirect("index.php");
                }
                else
                {
                    //invalid password
                    render("login_view.php", ["loginError" => true]);
                }
            }
            else
            {
                //invalid username
                render("login_view.php", ["loginError" => true]);
            }
        }

    }
?>
