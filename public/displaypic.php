<?php
    require("../includes/config.php");

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {


        if(isset($_POST["logout"]))
        {
            logout();
            redirect("index.php");
        }

        //if upload pressed without any file selected
        if (isset($_POST["profilepic"]) && $_FILES["picture"]["error"] == 4)
        {
            /*redirect to homepage with all the data plus a message flag
            that would tell to select a file to upload*/
            display_profilepic_error("No File Was Selected");
        }
        //bigger file uploaded than allowed by php.ini
        //else if (isset($_POST["profilepic"]) && $_FILES["picture"]["size"] >  8388608)
        //{
            /* bug THIS IS NOT WORKING NEED TO WORK ON THIS */
        //    display_profilepic_error("File too big");
        //}
        else if (isset($_POST["profilepic"]))
        {
            print_r($_FILES);
            if($_FILES["picture"]["type"] == "image/jpeg"
                || $_FILES["picture"]["type"] == "image/jpg"
                || $_FILES["picture"]["type"] == "image/png"
                && $_FILES["picture"]["error"] != 2 && $_FILES["picture"]["size"] < 200000)
            {
                //print_r($_FILES);
                $uploads = "/uploads/";
                //get username of user
                $username = getusername();

                //getting the mime type jpg,jpeg or png
                $extension = substr($_FILES["picture"]["type"], (strripos($_FILES["picture"]["type"], "/")) + 1);

                //changing the name of the file that the use provided as username.fileextension
                $_FILES["picture"]["name"] = "{$username}" . "." . "{$extension}";

                $filename = $_FILES["picture"]["name"];
                //print(basename($_FILES["picture"]["name"]));

                //file location relative to public that is inside public as /uploads/furrukh.jpeg
                $filelocation = $uploads . basename($_FILES["picture"]["name"]);


                //print(__DIR__ . $filelocation);
                /*__DIR__ IS USED TO GET DOCUMENT ROOT e:xammp/htdocs/prediction/public plus /uploads/$filename.$extension*/
                $destination = __DIR__ . $filelocation;

                try
                {
                    //check if a row already exists for this user in settings table
                    $sql = $dbh->prepare("SELECT * FROM settings WHERE user=?");
                    $sql->execute([$username]);
                    $row = $sql->fetch(PDO::FETCH_ASSOC);
                    if(empty($row))
                    {
                        //no entry with this username in db yet so add username as well and image as username.extension
                        $sql = $dbh->prepare("INSERT INTO settings (user, profile_image) VALUES (?,?)");
                        $sql->execute([$username, $filename]);

                        //reload homepage
                        redirect("index.php");

                    }
                    else
                    {
                        //user already there check if user has a profile pic set, that will be there in $row
                        if(empty($row["profile_image"]))
                        {
                            //update profile_image column
                            $sql = $dbh->prepare("UPDATE settings set profile_image = ? WHERE user = ?");
                            $sql->execute([$filename, $username]);
                            //move uploaded file to uploads
                            move_uploaded_file($_FILES['picture']['tmp_name'], $destination );

                            //reload homepage
                            redirect("index.php");
                        }
                        else
                        {
                            $image_location = __DIR__ . "/uploads/" . "{$row["profile_image"]}";
                            //print($image_location);
                            //update profile pic in db
                            $sql = $dbh->prepare("UPDATE settings set profile_image = ? WHERE user = ?");
                            $sql->execute([$filename, $username]);

                            //delete the already present profile pic
                            unlink($image_location);

                            //move the new uploaded file to uploads
                            move_uploaded_file($_FILES['picture']['tmp_name'], $destination );

                            //reload homepage
                            redirect("index.php");
                        }
                    }


                }
                catch (Exception $e)
                {
                    display_profilepic_error("something went wrong try again");
                }

            }
            else
            {
                //file bigger than allowed limit from form max size file
                if($_FILES["picture"]["error"] == 2)
                {
                    display_profilepic_error("Please select a file less than 200KB");
                }
                else
                {
                    /*INVALID FILE FORMAT */
                    display_profilepic_error("Invalid file Format");
                }
            }
        }
    }
?>
