<?php
    //connecting to db
    $dsn = "sqlite:predictions.db";

    try
    {
        $dbh = new PDO($dsn);
        $dbh->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e)
    {
        print("could not connect to db" .$e->getMessage());
    }

    //getting all the passwords
    $rows = $dbh->query("SELECT * FROM passwords WHERE used = 'no'");

    $filehandle  = fopen("password.txt", "w");
    foreach($rows as $row)
    {
        print($row["password"]. "\n");
        fwrite($filehandle, "{$row["password"]}" .PHP_EOL);
    }
    fclose($filehandle);

?>
