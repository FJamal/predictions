<div class="title">
    <div id="image">
        <div id = "headline">
            <h1>My Predictions</h1>
        </div>
    </div>
    <div id = "image2">
        <h4>predict the future!</h4>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div id ="register">
                <form class="form-signin" action="register.php" method="post">
                    <h2 class="form-signin-heading">Register Here</h2>
                    <label for="Name" class="sr-only">Name</label>
                    <input name = "name" type="text"  class="form-control" placeholder="Full Name" required autofocus>
                    <label for="Email" class="sr-only">Email address</label>
                    <input name = "email" type="email" class="form-control" placeholder="Email ID" required>
                    <label for="Username" class="sr-only">Username</label>
                    <input id ="username" name = "username" type="text" class="form-control" placeholder="Username" required>

                    <label for="Password" class="sr-only">Password</label>
                    <input id = "password" name = "password" type="password" class="form-control" placeholder="Password" required>
                    <label for="Confirm Password " class="sr-only">Confirm Password</label>
                    <input id = "confirmation" name = "confirmation" type="password" class="form-control" placeholder="Confirm Password" required>
                    <input name = "pinpassword" type="password" class="form-control" placeholder="Admin Provided Pin Password" required>
                    <?php if(isset($servervalidation)): ?>
                        <h5 style="color : yellow"><?=$servervalidation ?></h5>
                    <?php endif; ?>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                </form>
            </div>
        </div>

    </div>

</div>

<script>
    $(document).ready(function()
    {
        $("#password").on("focus", function ()
        {
            //remove password error message if there was any
            var passworderrormsg = document.getElementsByTagName('p');
            if(passworderrormsg.length > 0)
            {
                for(var i = 0; i < passworderrormsg.length; i++)
                {
                    if(passworderrormsg[i].getAttribute("id") == "usernameerror")
                    {
                        continue;
                    }
                    else
                    {
                        passworderrormsg[i].remove();
                    }

                }
            }



            var p = document.createElement('p');
            var text = document.createTextNode("password should be atleast 6 characters long");
            p.appendChild(text);

            //adding this msg below password field
            $("#password").after(p);

        });
        /*Password field on blur*/
        $("#password").on("blur", function ()
        {
            //remove the tip message
            $("#password").next().remove();

            var passwordlength = this.value.length;

            if(passwordlength > 0 && passwordlength < 6 )
            {


                //add error message that password is less than 6 char long
                var p = document.createElement('p');
                var text = document.createTextNode("password is less than 6 characters long");
                p.appendChild(text);
                //adding this msg below password field
                $("#password").after(p);

                //add css to it
                $("#password").next().css(
                {
                    "color" : "yellow"
                });

                //disabling the button
                $("button").attr("disabled", "disabled");

            }
            else
            {
                //enable the button if username error not there
                if(!$("#usernameerror"))
                {
                    $("button").removeAttr("disabled");
                }

            }

        });

        /*Checking both passwords */
        $("#confirmation").on("blur", function ()
        {
            var password = $("#password").val();
            var confirmation = $("#confirmation").val();

            var passwordmatcherror = document.getElementsByTagName('p');
            if (passwordmatcherror.length > 0)
            {
                //means password error msg is displayed
                for(var i = 0; i < passwordmatcherror.length; i++)
                {
                    if(passwordmatcherror[i].getAttribute("id") == "usernameerror")
                    {
                        continue;
                    }
                    else
                    {
                        passwordmatcherror[i].remove();
                    }
                }
            }

            if (password == confirmation)
            {
                //restore the button only when password matches and usernameerror is not already there
                if ($("button").attr("disabled") == "disabled")
                {
                    console.log("INN");
                    if(!document.getElementById('usernameerror'))
                    {
                        console.log("INNnn");
                        $("button").removeAttr("disabled");
                    }
                    //restore submit button
                    //$("button").removeAttr("disabled");
                }
            }
            else
            {

                //add error message that password dont match
                var p = document.createElement('p');
                var text = document.createTextNode("passwords dont match");
                p.appendChild(text);

                $("#confirmation").after(p);
                //add css to it
                $("#confirmation").next().css(
                {
                    "color" : "yellow"
                });

                //set register button to disabled
                $("button").attr("disabled", "disabled");
            }
        });


        /*Checking username*/
        $("#username").on("blur", function ()
        {
            //get the tip message and remove it
            $("#tipusername").remove();

            var username = $("#username").val();
            //checking via ajax if username exist
            $.ajax({
                type : "POST",
                url : "ajax-username.php",
                data : {"username" : username},
                success : function (data)
                        {
                            console.log(data);
                            if(data.username)
                            {
                                //true means user name already in db
                                //display msg to change username
                                var p = document.createElement("p");
                                var text = document.createTextNode("username already taken please select another username");
                                p.appendChild(text);

                                //add msg
                                $("#username").after(p);
                                $("#username").next().attr("id", "usernameerror");
                                //add css to it
                                $("#usernameerror").css({
                                    "color" : "yellow"
                                });

                                //disabling the button
                                $("button").attr("disabled", "disabled");

                            }
                        },
                fail : function()
                        {
                            console.log("error");
                        }

            });
        });

        $("#username").on("focus", function ()
        {
            //remove username error if there is any
            $("#usernameerror").remove();
            var p = document.createElement("p");
            var text = document.createTextNode("usernames are unique, create one that represents you, they can not be changed later");
            p.appendChild(text);

            $("#username").after(p);
            $("#username").next().attr("id", "tipusername");

            //restore button only if there is no other error
            var p = document.getElementsByTagName('p');
            if(p.length == 1)
            {
                $("button").removeAttr("disabled");
            }

        })
    });

</script>
