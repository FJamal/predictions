<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/css/main.css">
        <title>My Predictions</title>
    </head>
    <body>
        <div class="title">
            <div id="image">
                <div id = "headline">
                    <h1>My Predictions</h1>
                </div>
            </div>
            <div id = "image2">
                <h4>predict the future!</h4>
            </div>
        </div>
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-8">
                    <div id="signinpanel">
                      <form class="form-signin" action="login.php" method="POST">
                        <h2 class="form-signin-heading">Please sign in</h2>
                        <label for="Username" class="sr-only">Email address</label>
                        <input name = "user" type="text" id="username" class="form-control" placeholder="Username" required autofocus>

                        <label for="inputPassword" class="sr-only">Password</label>
                        <input name = "password" type="password" id="password" class="form-control" placeholder="Password" required>
                        <?php if(isset($loginError)): ?>
                            <p id ="loginerror">Invalid username or password</p>
                        <?php endif; ?>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                        <a href="register.php">Register</a>
                    </form>


                    </div>
                </div>

            </div>


        </div>
    </body>
</html>
