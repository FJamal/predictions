<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/css/main.css">

        <!-- CDN for Jquery -->
        <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script> -->

        <script src = "../js/jquery-3.3.1.min.js">

        </script>

        <title><?= $title ?></title>
        <style>
        /*different css based on if trainers is set in settings to display different
        formatted divs for get views, added this here cuz of dynamism, .css file dont
        recognize php tags*/

        <?php if(isset($trainer)):?>
        #sector1 , #sector6
        {
            background-color: yellow;
            height: 100px;
        }
        #sector2, #sector7
        {
            background-color: red;
            height: 100px;
        }
        #sector3 , #sector8
        {
            background-color: green;
            height: 100px;
        }
        #sector4
        {
            background-color: brown;
            height: 200px;
        }
        #video
        {
            
            height: 200px;
        }


        #sector5
        {
            background-color: grey;
            height: 200px;
        }
        <?php else: ?>
        #sector1 , #sector6
        {
            height: 100px;
        }
        #sector2, #sector7
        {
            height: 100px;
        }
        #sector3 , #sector8
        {
            height: 100px;
        }
        #sector4
        {
            height: 200px;
        }
        #video
        {
            height: 200px;
        }
        #sector5
        {
            height: 200px;
        }
        <?php endif; ?>

        </style>
    </head>
    <body>
